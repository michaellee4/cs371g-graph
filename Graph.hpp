// -------
// Graph.h
// --------

#ifndef Graph_h
#define Graph_h

// --------
// includes
// --------

#include <cassert> // assert
#include <cstddef> // size_t
#include <utility> // make_pair, pair
#include <vector>  // vector
#include <unordered_map>
#include <unordered_set>

// -----
// Graph
// -----

class Graph {
public:
    // ------
    // usings
    // ------

    /**
     * Type used for describing vertices
     */
    using vertex_descriptor  = int;

    /**
     * Type used for describing edges
     */
    using edge_descriptor    = std::pair<vertex_descriptor, vertex_descriptor>;

    /**
     * Type used for describing number of vertices
     */
    using vertices_size_type = std::size_t;

    /**
     * Type used for describing number of edges
     */
    using edges_size_type    = std::size_t;

private:
    /**
     * A custom hashing object for edge_descriptors (std::pair<int, int>). Which will allow us to store edges in an unordered_{set|map}
     */
    struct edge_hasher {
        /**
         * Hashing function from: https://stackoverflow.com/questions/45395071/hash-for-a-stdpair-for-use-in-an-unordered-map
         */
        std::size_t operator()(const edge_descriptor& x) const
        {
            static_assert(sizeof(int) * 2 == sizeof(std::size_t));
            return std::size_t(std::hash<vertex_descriptor>()(x.first)) << (32) | std::hash<vertex_descriptor>()(x.second);
        }
    };

    /**
     * Type used for describing the internal container storing edges
     * Is somewhat redundant but we store edges separately to make iterating over edges easier.
     * Use an unordered set for fast look up
     */
    using edge_container     = std::unordered_set<edge_descriptor, edge_hasher>;

    /**
     * Type used for describing the internal container storing adjacent vertices to a given vertex
     */
    using adjacents_container   = std::unordered_set<vertex_descriptor>;

    /**
     * Type used for describing the internal container storing the underlying graph
     * Use a unordered_map of unoredered_sets for fast vertex/edge lookup and deletion
     * Key is vertex_descriptor, value is an unordered_set of vertex_descriptors representing adjacent vertices to the key edge
     */
    using graph_container    = std::unordered_map<vertex_descriptor, adjacents_container>;

    /**
     * A wrapper iterator around an unoredered_map::iterator that only returns the key rather than a pair.
     */
    class vertex_iter : public graph_container::const_iterator
    {
    public:
        vertex_iter() : graph_container::const_iterator() {};
        vertex_iter(const graph_container::const_iterator& s) : graph_container::const_iterator(s) {};
        /**
         * Return just the key
         */
        const vertex_descriptor* operator->() {
            return &(graph_container::const_iterator::operator->()->first);
        }

        /**
         * Return just the key
         */
        vertex_descriptor operator*() {
            return graph_container::const_iterator::operator*().first;
        }
    };

public:
    /**
     * Type used for describing the iterator over vertex descriptors in this graph
     */
    using vertex_iterator    = vertex_iter;

    /**
     * Type used for describing the iterator over edge descriptors in this graph
     */
    using edge_iterator      = edge_container::const_iterator;

    /**
     * Type used for describing the iterator storing adjacent vertices in this graph
     */
    using adjacency_iterator = adjacents_container::const_iterator;

public:
    // --------
    // add_edge
    // --------

    /**
     * @param s   The vertex descriptor of the source vertex in the edge to add
     * @param d   The vertex descriptor of the destination vertex in the edge to add
     * @param g   The graph to add this edge to
     * @return    A pair where the first element is either the edge descriptor of the newly added edge or the edge descriptor of the pre-existing edge if it already existed. The second element is a boolean that is true if a new edge is added and false other wise.
     */
    friend std::pair<edge_descriptor, bool> add_edge (const vertex_descriptor s, const vertex_descriptor d, Graph& g) {
        const edge_descriptor ed = {s, d};
        bool success = false;
        if (g.edges_.find(ed) == g.edges_.end()) {
            success = true;
            g.edges_.insert(ed);
            g.g_[s].insert(d);
            g.g_[d];
        }
        return {ed, success};
    }

    // ----------
    // add_vertex
    // ----------

    /**
     * @param g   The graph to add a vertex to
     * @return    A vertex descriptor describing the newly added vertex that does not exist in the set of vertices prior to this operation.
     */
    friend vertex_descriptor add_vertex (Graph& g) {
        // <your code>
        vertex_descriptor v = g.next_vertex_++;
        assert(g.g_.find(v) == g.g_.end());
        g.g_.insert({v, {}});
        assert(g.g_.find(v) != g.g_.end());
        return v;
    }

    // -----------------
    // adjacent_vertices
    // -----------------

    /**
     * @param v   The vertex to get adjacent vertices to
     * @param g   The graph to get adjacent vertices from
     * @return    A pair if iterators describing the beginning and end of a range of vertices.
     */
    friend std::pair<adjacency_iterator, adjacency_iterator> adjacent_vertices (const vertex_descriptor v, const Graph& g) {
        // <your code>
        const adjacents_container& adjacents = g.g_.at(v);
        return {adjacents.begin(), adjacents.end()};
    }

    // ----
    // edge
    // ----

    /**
     * @param s   The vertex descriptor of the source vertex in the edge to check
     * @param d   The vertex descriptor of the destination vertex in the edge to check
     * @param g   The graph to check whether an edge exists between s and d
     * @return    A pair where the first element is either the edge descriptor of the existing edge with source == d and target == d or an arbitrary edge. The second element is a boolean that is true if an edge exists with source == s and target == d and false other wise.
     */
    friend std::pair<edge_descriptor, bool> edge (vertex_descriptor s, vertex_descriptor d, const Graph& g) {
        // <your code>
        const edge_descriptor ed = {s, d};
        return {ed, g.edges_.find(ed) != g.edges_.end()};
    }

    // -----
    // edges
    // -----

    /**
     * @param g   The graph to get the edges of
     * @return    A pair if iterators describing the beginning and end of a range of edges in the input graph.
     */
    friend std::pair<edge_iterator, edge_iterator> edges (const Graph& g) {
        // <your code>
        const edge_container& e = g.edges_;
        return {e.begin(), e.end()};
    }

    // ---------
    // num_edges
    // ---------

    /**
     * @param g   The graph to get the number of edges from
     * @return    The number of edges in this graph
     */
    friend edges_size_type num_edges (const Graph& g) {
        // <your code>
        return g.edges_.size();
    }

    // ------------
    // num_vertices
    // ------------

    /**
     * @param g   The graph to get the number of vertices from
     * @return    The number of vertices in this graph
     */
    friend vertices_size_type num_vertices (const Graph& g) {
        // <your code>
        return g.g_.size();
    }

    // ------
    // source
    // ------

    /**
     * @param e   An edge descriptor to get the source of
     * @param g   The graph associated with the edge descriptor used for getting the source
     * @return    The source vertex of the edge associated with this edge descriptor in this graph
     */
    friend vertex_descriptor source (edge_descriptor e, const Graph&) {
        // <your code>
        return e.first;
    }

    // ------
    // target
    // ------

    /**
     * @param e   An edge descriptor to get the target of
     * @param g   The graph associated with the edge descriptor used for getting the target
     * @return    The target vertex of the edge associated with this edge descriptor in this graph
     */
    friend vertex_descriptor target (edge_descriptor e, const Graph&) {
        // <your code>
        return e.second;
    }

    // ------
    // vertex
    // ------

    /**
     * @param s   The offset into the vertex list to return
     * @param g   The graph we're getting the vertex from
     * @return    The s'th vertex in this graph's vertex list
     */
    friend vertex_descriptor vertex (vertices_size_type s, const Graph& g) {
        // <your code>
        vertex_iterator itr = vertex_iterator(g.g_.cbegin());
        std::advance(itr, s);
        return *itr;
    }

    // --------
    // vertices
    // --------

    /**
     * @param g   The graph to get the vertices of
     * @return    A pair if iterators describing the beginning and end of a range of vertices in the input graph.
     */
    friend std::pair<vertex_iterator, vertex_iterator> vertices (const Graph& g) {
        // <your code>
        return {vertex_iterator(g.g_.cbegin()), vertex_iterator(g.g_.cend())};
    }

private:

    // ----
    // data
    // ----

    graph_container g_; // something like this
    edge_container edges_;
    vertex_descriptor next_vertex_ = 0;

    // -----
    // valid
    // -----

    /**
     * @return True if the Graph is in a valid state or not and false otherwise
     */
    bool valid () const {
        // <your code>
        if ((int) next_vertex_ < (int) g_.size()) return false;
        return true;
    }

public:
    // --------
    // defaults
    // --------

    Graph             ()             = default;
    Graph             (const Graph&) = default;
    ~Graph            ()             = default;
    Graph& operator = (const Graph&) = default;
};

#endif // Graph_h

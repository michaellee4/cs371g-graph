// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, add_edge_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    const auto res = add_edge(vdA, vdB, g);
    ASSERT_TRUE(res.second);
}

TYPED_TEST(GraphFixture, add_edge_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    const auto res = add_edge(vdA, vdB, g);
    ASSERT_TRUE(res.second);
    const auto res2 = add_edge(vdA, vdB, g);
    ASSERT_FALSE(res2.second);

    // Boost docs state that if second is false, then first is the edge descriptor of the existing edge
    ASSERT_EQ(res.first, res2.first);
}

TYPED_TEST(GraphFixture, add_edge_test3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    // allow cyclic graph
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    const auto res = add_edge(vdA, vdB, g);
    ASSERT_TRUE(res.second);
    const auto res2 = add_edge(vdB, vdA, g);
    ASSERT_TRUE(res2.second);
}

TYPED_TEST(GraphFixture, add_edge_test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    // allow cyclic graph
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [ed, added] = add_edge(vdA, vdB, g);
    ASSERT_TRUE(added);
    ASSERT_TRUE(edge(vdA, vdB, g).second);
    ASSERT_EQ(ed, edge(vdA, vdB, g).first);
}

TYPED_TEST(GraphFixture, add_vertex_test1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 1);
}

TYPED_TEST(GraphFixture, add_vertex_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_NE(vdA, vdB);
}

TYPED_TEST(GraphFixture, add_vertex_test3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
    std::vector<vertex_descriptor> vs;
    vs.push_back(vertex(0, g));
    vs.push_back(vertex(1, g));
    ASSERT_EQ(vs.size(), 2);
    ASSERT_TRUE(find(vs.begin(), vs.end(), vdA) != vs.end());
    ASSERT_TRUE(find(vs.begin(), vs.end(), vdB) != vs.end());
}

TYPED_TEST(GraphFixture, adjacent_vertices_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 1);

    auto [b, e] = adjacent_vertices(vdA, g);
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, adjacent_vertices_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [ed, added] = add_edge(vdA, vdB, g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_TRUE(added);

    auto [b, e] = adjacent_vertices(vdA, g);
    ASSERT_TRUE(b != e);
    ASSERT_EQ(std::distance(b, e), 1);
    ASSERT_EQ(*b, vdB);
}

TYPED_TEST(GraphFixture, adjacent_vertices_test3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [ed, added] = add_edge(vdA, vdB, g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_TRUE(added);

    auto [b, e] = adjacent_vertices(vdB, g);
    ASSERT_TRUE(b == e);
    ASSERT_EQ(std::distance(b, e), 0);
}

TYPED_TEST(GraphFixture, edge_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [orig_ed, added] = add_edge(vdA, vdB, g);

    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_TRUE(added);

    auto [new_ed, exists] = edge(vdA, vdB, g);
    ASSERT_TRUE(exists);
    ASSERT_EQ(orig_ed, new_ed);
}

TYPED_TEST(GraphFixture, edge_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);

    auto [ed, exists] = edge(vdA, vdB, g);
    ASSERT_FALSE(exists);
}

TYPED_TEST(GraphFixture, edges_test1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    auto [b, e] = edges(g);
    ASSERT_TRUE(b == e);
    ASSERT_EQ(std::distance(b, e), 0);
}

TYPED_TEST(GraphFixture, edges_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [ed, _] = add_edge(vdA, vdB, g);

    auto [b, e] = edges(g);
    ASSERT_FALSE(b == e);
    ASSERT_EQ(std::distance(b, e), 1);

    ASSERT_EQ(*b, ed);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, edges_test3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_edge(vdA, vdB, g);
    auto [ed, _] = add_edge(vdA, vdB, g);

    auto [b, e] = edges(g);
    ASSERT_FALSE(b == e);
    ASSERT_EQ(std::distance(b, e), 1);

    ASSERT_EQ(*b, ed);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, edges_test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    auto [ed1, tmp1] = add_edge(vdA, vdB, g);
    auto [ed2, tmp2] = add_edge(vdB, vdA, g);
    auto [ed3, tmp3] = add_edge(vdA, vdC, g);

    ASSERT_EQ(num_edges(g), 3);
    auto [b, e] = edges(g);
    ASSERT_FALSE(b == e);
    ASSERT_EQ(std::distance(b, e), 3);
    std::vector<edge_descriptor> vs {b, e};
    ASSERT_TRUE(std::find(vs.begin(), vs.end(), ed1) != vs.end());
    ASSERT_TRUE(std::find(vs.begin(), vs.end(), ed2) != vs.end());
    ASSERT_TRUE(std::find(vs.begin(), vs.end(), ed3) != vs.end());
}

TYPED_TEST(GraphFixture, num_edges_test1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, num_edges_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, num_edges_test3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 1000; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        vertex_descriptor vdB = add_vertex(g);
        add_edge(vdA, vdB, g);
    }
    ASSERT_EQ(num_edges(g), 1000);
}

TYPED_TEST(GraphFixture, num_edges_test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    for (int i = 0; i < 1000; ++i) {

        add_edge(vdA, vdB, g);
    }
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, num_vertices_test1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(num_vertices(g), 0);
}

TYPED_TEST(GraphFixture, num_vertices_test2) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    ASSERT_EQ(num_vertices(g), 2);
}

TYPED_TEST(GraphFixture, num_vertices_test3) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 1000; ++i) {
        add_vertex(g);
    }

    ASSERT_EQ(num_vertices(g), 1000);
}

TYPED_TEST(GraphFixture, source_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [ed, _] = add_edge(vdA, vdB, g);
    ASSERT_EQ(source(ed, g), vdA);
}

TYPED_TEST(GraphFixture, source_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    (void) vdA;
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    auto [ed, _] = add_edge(vdC, vdB, g);
    ASSERT_EQ(source(ed, g), vdC);
}

TYPED_TEST(GraphFixture, target_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    auto [ed, _] = add_edge(vdA, vdB, g);
    ASSERT_EQ(target(ed, g), vdB);
}

TYPED_TEST(GraphFixture, target_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    (void) vdA;
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    auto [ed, _] = add_edge(vdC, vdB, g);
    ASSERT_EQ(target(ed, g), vdB);
}

TYPED_TEST(GraphFixture, vertex_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor v0  = vertex(0, g);
    ASSERT_TRUE(vdA == v0 || vdB == v0);
}

TYPED_TEST(GraphFixture, vertices_test1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    auto [b, e] = vertices(g);
    ASSERT_TRUE(b == e);
    ASSERT_EQ(std::distance(b, e), 0);
}

TYPED_TEST(GraphFixture, vertices_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    auto [b, e] = vertices(g);
    ASSERT_FALSE(b == e);
    ASSERT_EQ(std::distance(b, e), 1);

    ASSERT_EQ(*b, vdA);
    ++b;
    ASSERT_TRUE(b == e);
}

TYPED_TEST(GraphFixture, vertices_test3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    ASSERT_EQ(num_vertices(g), 3);
    auto [b, e] = vertices(g);
    ASSERT_FALSE(b == e);
    ASSERT_EQ(std::distance(b, e), 3);
    std::vector<vertex_descriptor> vs {b, e};
    ASSERT_TRUE(std::find(vs.begin(), vs.end(), vdA) != vs.end());
    ASSERT_TRUE(std::find(vs.begin(), vs.end(), vdB) != vs.end());
    ASSERT_TRUE(std::find(vs.begin(), vs.end(), vdC) != vs.end());
}
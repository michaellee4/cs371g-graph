# CS371g: Generic Programming Graph Repo

* Name: (your Full Name)
    - Michael Lee

* EID: (your EID)
    - ml45898

* GitLab ID: (your GitLab ID)
    - michaellee4

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)
    - 519dc27ed70e7a4b3329e23ba05698a2316745a2

* GitLab Pipelines: (link to your GitLab CI Pipeline)
    - https://gitlab.com/michaellee4/cs371g-graph/-/pipelines

* Estimated completion time: (estimated time in hours, int or float)
    - 10 hours

* Actual completion time: (actual time in hours, int or float)
    - 6 hours

* Comments: (any additional comments you have)
